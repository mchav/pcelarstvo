﻿
using Pcelarstvo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MongoDB.Linq;
using MongoDB;

namespace Pcelarstvo.Controllers
{
    public class SandaciController : Controller
    {
        //
        // GET: /Sandaci/

        public ActionResult Index()
        {
            //Create a default mongo object.  This handles our connections to the database.
            //By default, this will connect to localhost, port 27017 which we already have running from earlier.
            var mongo = new Mongo();
            mongo.Connect();

            //Get the blog database.  If it doesn't exist, that's ok because MongoDB will create it 
            //for us when we first use it. Awesome!!!
            var db = mongo.GetDatabase("Pcelarstvo");

            //Get the Post collection.  By default, we'll use the name of the class as the collection name. Again,
            //if it doesn't exist, MongoDB will create it when we first use it.
            var collection = db.GetCollection<Sandak>();

            var model = from p in collection.Linq()
                        select new Sandak
                        {
                            id = p.id,
                            naziv = p.naziv
                        };  

            return View(model.AsEnumerable());
        }



     

     

    


        public ActionResult Create()
        {
            var mongo = new Mongo();
            mongo.Connect();
            var db = mongo.GetDatabase("Pcelarstvo");
            var collection = db.GetCollection<Sandak>();


            var sandak = new Sandak()
            {
                id = 1,
                naziv = "sandak 1",
                imaMatica = true
            };

            collection.Save(sandak);

            return View();
        }

        public ActionResult Edit()
        {


            return View();
        }


        public ActionResult Delete()
        {


            return View();
        }

    }
}
