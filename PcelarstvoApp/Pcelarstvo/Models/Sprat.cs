﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Pcelarstvo.Models
{
    /// <summary>
    /// Sprat list entity
    /// </summary>
    public class Sprat
    {
        public int id;
        public int sandakID;
        public int brojNaRamki;

        public IList<Ramka> Ramki { get; set; }

    }

}