﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Pcelarstvo.Models
{
    /// <summary>
    /// EvidencijaMed list entity
    /// </summary>
    public class EvidencijaMed
    {
        public int ramkaID;
        public DateTime vadenaNADatum;
        public int sandakID;
    }

}