﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Pcelarstvo.Models
{
    /// <summary>
    /// Ramka list entity
    /// </summary>
    public class Ramka
    {
        public int id;
        public int tipNaRamka;
        public int spratId;
    }

}