﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Pcelarstvo.Models
{
    /// <summary>
    /// Sandak list entity
    /// </summary>
    public class Sandak
    {
        public int id;
        public int spratovi;
        public int spratId;
        public bool imaMatica;
        public DateTime datumKogaEIzvedenaMaticata;
        public string naziv;

        public IList<Sprat> Spratovi { get; set; }
        public IList<EvidencijaMed> EvidencijaMed { get; set; }

    }

}